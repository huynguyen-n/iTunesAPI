//
//  TrackConfiguration.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation

class TrackConfiguration {
 
    static let shared = TrackConfiguration()
    
    func configue(viewController: TrackController) {
        let presenter = TrackPresenter()
        presenter.output = viewController
        
        let interactor = TrackInteractor()
        interactor.output = presenter
        
        viewController.output = interactor
        viewController.dataSource = presenter
    }
}
