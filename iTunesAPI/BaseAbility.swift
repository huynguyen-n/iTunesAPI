//
//  BaseAbility.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation

protocol BaseAbility: class {
    
    func initBaseAbility()
    
    func initCommons()
    
    func initUIs()
    
    func initBinding()
    
    func initActions()
}

extension BaseAbility {
    
    func initBaseAbility() {
        self.initCommons()
        self.initUIs()
        self.initBinding()
        self.initActions()
    }
}
