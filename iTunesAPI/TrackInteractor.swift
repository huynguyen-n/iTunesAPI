//
//  TrackInteractor.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit
import PromiseKit

protocol TrackInteractorOutput {
    
    func presenterError(_ error: NSError)
}

class TrackInteractor {
    
    var output: TrackInteractorOutput?
    
    fileprivate lazy var fetchTrackWorker: FetchTrackWorker = {
       return FetchTrackWorker()
    }()
}

extension TrackInteractor: TrackControllerOutput {

    func fetchList() {
        self.fetchTrackWorker.excute()
            .then { _ -> Void in
            
            }.catch { error in
                self.output?.presenterError(error as NSError)
        }
    }
    
    func fetchList(withText text: String? = "") {
        self.fetchTrackWorker.searchTrack(withText: text!)
            .then { _ -> Void in
                
            }.catch { error in
                self.output?.presenterError(error as NSError)
        }
    }
}
