//
//  BaseObj.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class TrackObj: Mappable {

    var trackName: String?
    var previewUrl: String?
    
    required init?(map: Map) {
        //
    }
    
    func mapping(map: Map) {
        trackName <- map[Constants.ObjModel.Track.TrackName]
        previewUrl <- map[Constants.ObjModel.Track.PreviewURL]
    }
}
