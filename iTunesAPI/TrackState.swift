
//
//  TrackReducer.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import ReSwift
import RxSwift


class TrackState: StateType {
    
    var tracks = Variable<[TrackObj]>([])
}

extension TrackState {
    static func reducer(action: Action, state: TrackState?) -> TrackState {
        
        // Get state
        let state = state ?? TrackState()
        
        // Doing
        switch action {
        case let action as UpdateTrackListAct:
            
            /// Update here
            state.tracks.value = action.tracks ?? []
            
            break
        default:
            break
        }
        
        return state
    }
}
