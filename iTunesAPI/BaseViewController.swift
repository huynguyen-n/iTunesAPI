//
//  BaseViewController.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit

extension UIViewController: BaseAbility {
    
    func initCommons() {
        
    }
    
    func initActions() {
        
    }
    
    func initUIs() {
        
    }
    
    func initBinding() {
        
    }
}
