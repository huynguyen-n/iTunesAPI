//
//  Worker.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import PromiseKit

protocol MainWorker {
    
}

protocol AsyncWorker: MainWorker {
    
    associatedtype T
    
    func excute() -> Promise<T>
}

protocol SyncWorker: MainWorker {
    func excute()
}
