//
//  Identifiers.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import UIKit

protocol Identifier {
    
    static var identifierView: String { get }
    
    static func xib() -> UINib?
}
