//
//  TrackDataSource.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import RxSwift
import UIKit
import AudioPlayer

protocol TrackDataSourceDelegate: class {
    
    // Reload
    func reloadTableView()
}

class TrackDataSource: NSObject {
    
    weak var delegate: TrackDataSourceDelegate?
    private let disableBag = DisposeBag()
    fileprivate var tracks: Variable<[TrackObj]> {
        return mainStore.state.trackState!.tracks
    }
    
    override init() {
        super.init()
        
        self.tracks.asObservable().subscribe{ [unowned self] (repo) in
            self.delegate?.reloadTableView()
        }.addDisposableTo(disableBag)
    }
}

extension TrackDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackCell", for: indexPath) as! TrackCell
        cell.configCell(track: self.tracks.value[indexPath.row])
        return cell
    }
}

extension TrackDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let track = self.tracks.value[indexPath.row]
        do {
            
            let audioPlayer = try AudioPlayer(contentsOf: URL(string: track.previewUrl!)!)
            
            // Start playing
            audioPlayer.play()
            // Stop playing with a fade out
            audioPlayer.fadeOut()
            
        } catch let error as NSError { error.debugDescription }
    }
}
