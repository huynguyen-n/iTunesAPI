//
//  MainReducer.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import ReSwift

struct MainReducer {
    
}

extension MainReducer: Reducer {
    
    func handleAction(action: Action, state: MainAppState?) -> MainAppState {
        let trackState = TrackState.reducer(action: action, state: state?.trackState)
        return MainAppState(trackState: trackState)
    }
}
