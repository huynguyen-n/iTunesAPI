//
//  Networking.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Alamofire
import ObjectMapper
import PromiseKit

class Networking {
    
    static let shared = Networking()
    
    func fetchTrackWithText(text: String) -> Promise<[TrackObj]> {
        let trackObj = FetchTrackList(param: [Constants.APIKey.Term: text, Constants.APIKey.Entity: "song"])
        return trackObj.toPromise()
    }
}
