//
//  UIView+Identifier.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit

extension UIView: Identifier {
    
    
    /// ID View
    static var identifierView: String {
        get {
            return String(describing: self)
        }
    }
    
    
    /// XIB
    static func xib() -> UINib? {
        return UINib(nibName: self.identifierView, bundle: nil)
    }
}
