//
//  FetchTrackWorker.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit
import PromiseKit
import ReSwift

struct UpdateTrackListAct: Action {
    var tracks: [TrackObj]?
}

class FetchTrackWorker: AsyncWorker {
    
    typealias T = [TrackObj]
    
    func excute() -> Promise<T> {
        return Networking.shared.fetchTrackWithText(text: "jack+johnson")
            .then { (trackObjs) -> Promise<T> in
                
                let action = UpdateTrackListAct(tracks: trackObjs)
                mainStore.dispatch(action)
                
                return Promise(value: trackObjs)
                
        }
    }
    
    func searchTrack(withText text: String? = "") -> Promise<T> {
        return Networking.shared.fetchTrackWithText(text: text!)
            .then { (trackObjs) -> Promise<T> in
                
                let action = UpdateTrackListAct(tracks: trackObjs)
                mainStore.dispatch(action)
                
                return Promise(value: trackObjs)
        }
    }
}
