//
//  TrackPresentation.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit

protocol TrackPresenterOutput: class {
    
    func presentError(_ error: NSError)
    
    func reloadTableView()
}

class TrackPresenter {
    
    fileprivate lazy var trackDataSource: TrackDataSource = {
       
        let track = TrackDataSource()
        track.delegate = self
        return track
    }()
    
    weak var output: TrackPresenterOutput?
}

extension TrackPresenter: TrackInteractorOutput {
    
    func presenterError(_ error: NSError) {
        self.output?.presentError(error)
    }
}

extension TrackPresenter: TrackControllerDatasource {
    
    func delegate() -> UITableViewDelegate {
        return self.trackDataSource
    }

    func dataSource() -> UITableViewDataSource {
        return self.trackDataSource
    }
}

extension TrackPresenter: TrackDataSourceDelegate {
    func reloadTableView() {
        self.output?.reloadTableView()
    }
}
