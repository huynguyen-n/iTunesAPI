//
//  MainAppState.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Foundation
import ReSwift

struct MainAppState: StateType {
    
    //
    // MARK: - States
    
    /// Repo State
    let trackState: TrackState?
    
}
