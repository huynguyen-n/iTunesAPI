//
//  ViewController.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit
import RxSwift

protocol TrackControllerOutput {
    
    func fetchList()
    
    func fetchList(withText text: String?)
}

protocol TrackControllerDatasource: class {
    func dataSource() -> UITableViewDataSource
    
    func delegate() -> UITableViewDelegate
}

class TrackController: UIViewController {
    
    var output: TrackControllerOutput?
    weak var input: TrackPresenterOutput?
    weak var dataSource: TrackControllerDatasource?

    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    var searchText: String? {
        didSet {
            searchBar.text = searchText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TrackConfiguration.shared.configue(viewController: self)
        
        self.initBaseAbility()
        
        self.output?.fetchList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func initUIs() {
        self.tableView.registerCell(TrackCell.self)
        self.tableView.dataSource = self.dataSource?.dataSource()
        self.tableView.delegate = self.dataSource?.delegate()
    }
}

extension TrackController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.output?.fetchList(withText: searchText)
    }
}

extension TrackController: TrackPresenterOutput {
    
    func presentError(_ error: NSError) {
        
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
}

