//
//  BaseView.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import UIKit

extension UIView: BaseAbility {
    
    func initCommons() {
        
    }
    
    func initBinding() {
        
    }
    
    func initUIs() {
        
    }
    
    func initActions() {
        
    }
}
