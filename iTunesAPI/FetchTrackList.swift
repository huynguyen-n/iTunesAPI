//
//  FetchTrackList.swift
//  iTunesAPI
//
//  Created by Huy Nguyen on 4/1/17.
//  Copyright © 2017 Huy Nguyen. All rights reserved.
//

import Alamofire
import ObjectMapper

struct FetchTrackList: Requestable {
    
    typealias T = [TrackObj]
    
    var param: Parameters?
    
    var httpMethod: HTTPMethod { get { return .get } }
    
    var endPoint: String { get { return Constants.APIEndpoint.Search } }
    
    var parameterEncoding: ParameterEncoding { get { return URLEncoding.default } }
    
    func decode(data: Any) -> Array<TrackObj> {
        let arr = data as? [String: Any]
        return Mapper<TrackObj>().mapArray(JSONObject: arr?["results"])  ?? []
    }
    
    init(param: Requestable.Parameters?) {
        self.param = param
    }
}
